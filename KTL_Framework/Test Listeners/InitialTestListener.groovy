import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
//import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.keyword.excel.ExcelKeywords
//import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext

import com.kms.katalon.core.util.KeywordUtil
import com.extosoft.keyword.UtilityKeyword
import com.extosoft.keyword.ExcelKeyword

import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.*
import org.apache.poi.ss.util.CellReference
import org.apache.poi.ss.util.WorkbookUtil

import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook

class InitialTestListener {
	
	
	def UtilityKeyword util = null
	def ExcelKeyword excelKw = null
	def XSSFWorkbook xssfWb = null
	def Sheet sh = null
	def XSSFRow xssfRw = null;
	
	/**
	 * Executes before every test case starts.
	 * @param testCaseContext related information of the executed test case.
	 */
	@BeforeTestCase
	def sampleBeforeTestCase(TestCaseContext testCaseContext) {
//		println testCaseContext.getTestCaseId()
//		println testCaseContext.getTestCaseVariables()
		
		KeywordUtil.logInfo("********* SampleBeforeTestCase " + testCaseContext.getTestCaseId() + " : " + testCaseContext.getTestCaseStatus() + " *********")
		xssfWb = excelKw.getWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		String testCaseId = testCaseContext.getTestCaseId()
		KeywordUtil.logInfo("TestCaseId : " + testCaseId)
		GlobalVariable.currentTestCaseName = testCaseId.substring(testCaseId.indexOf('/') + 1)
//		WebUI.comment("#sampleBeforeTestCase GlobalVariable.currentTestCaseName=${GlobalVariable.currentTestCaseName}")
		GlobalVariable.testCaseExecuteStatus = "NOT EXECUTED"
		GlobalVariable.runningStep = 1
		excelKw = new ExcelKeyword()
		xssfWb = excelKw.getWorkbook(GlobalVariable.excelController)
		KeywordUtil.logInfo("Get ExcelController : " + GlobalVariable.excelController)
		sh = excelKw.getExcelSheet(xssfWb, "TestController")
		KeywordUtil.logInfo("Get SheetName : " + sh.getSheetName())
		KeywordUtil.logInfo("Get currentRowTestCase : " + GlobalVariable.currentRowTestCase)
		
		xssfRw = sh.getRow(GlobalVariable.currentRowTestCase)
		
		XSSFCell testCase = xssfRw.getCell(0)
		XSSFCell testName = xssfRw.getCell(1)
		XSSFCell tag = xssfRw.getCell(2)
		
		GlobalVariable.testCase =  testCase.getStringCellValue()
		GlobalVariable.testCaseName =  testName.getStringCellValue()
		GlobalVariable.tag = tag.getStringCellValue()
		
		KeywordUtil.logInfo("Get TestCase : " + GlobalVariable.testCase)
		KeywordUtil.logInfo("Get TestCaseName : " + GlobalVariable.testCaseName)
		KeywordUtil.logInfo("Get Tag : " + GlobalVariable.tag)

		if(GlobalVariable.tag == "No Run"){
			KeywordUtil.logInfo("SkipThisTestCase : " + GlobalVariable.testCase)
			xssfWb = excelKw.getWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
			KeywordUtil.logInfo("ReportResult Open : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
			sh = excelKw.getExcelSheet(xssfWb, "TestController")
			KeywordUtil.logInfo("Set TestCase ExecuteStatus : " + GlobalVariable.testCaseExecuteStatus)
			excelKw.setValueToCellByIndex(sh, GlobalVariable.currentRowTestCase, 3, GlobalVariable.testCaseExecuteStatus)
			excelKw.saveWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName, xssfWb)
			KeywordUtil.logInfo("ReportResult Save : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
			//GlobalVariable.currentRowTestCase = GlobalVariable.currentRowTestCase + 1
			KeywordUtil.logInfo("********************************************")
			testCaseContext.skipThisTestCase()
		}
		
		KeywordUtil.logInfo("********************************************")
	}

	/**
	 * Executes after every test case ends.
	 * @param testCaseContext related information of the executed test case.
	 */
	@AfterTestCase
	def sampleAfterTestCase(TestCaseContext testCaseContext) {
//		println testCaseContext.getTestCaseId()
//		println testCaseContext.getTestCaseStatus()
		
		KeywordUtil.logInfo("********* SampleAfterTestCase " + testCaseContext.getTestCaseId() + " : " + testCaseContext.getTestCaseStatus() + " *********")
		if(GlobalVariable.tag == "No Run"){
			KeywordUtil.logInfo("SkipThisTestCase : " + GlobalVariable.testCase)
			GlobalVariable.currentRowTestCase = GlobalVariable.currentRowTestCase + 1
			KeywordUtil.logInfo("********************************************")
		}else{
			xssfWb = excelKw.getWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
			KeywordUtil.logInfo("ReportResult Open : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
			
			String testCaseStatus = testCaseContext.getTestCaseStatus()
			KeywordUtil.logInfo("Get TestCaseStatus : " + testCaseStatus)
			GlobalVariable.testCaseExecuteStatus = testCaseStatus
			
			if (testCaseStatus == "PASSED"){
				GlobalVariable.totalTestCasesPassed = GlobalVariable.totalTestCasesPassed + 1
			}else if(testCaseStatus == "FAILED"){
				GlobalVariable.totalTestCasesFailed = GlobalVariable.totalTestCasesFailed + 1
			}
	
			sh = excelKw.getExcelSheet(xssfWb, "TestController")
			KeywordUtil.logInfo("Get SheetName : " + sh.getSheetName())
			KeywordUtil.logInfo("Get CurrentRowTestCase : " + GlobalVariable.currentRowTestCase)
			
			GlobalVariable.totalTestCasesExecuted  = GlobalVariable.totalTestCasesExecuted + 1
			KeywordUtil.logInfo("TotalTestCasesExecuted : " + GlobalVariable.totalTestCasesExecuted)
			KeywordUtil.logInfo("TotalTestCasesPassed : " + GlobalVariable.totalTestCasesPassed)
			KeywordUtil.logInfo("TotalTestCasesFailed : " + GlobalVariable.totalTestCasesFailed)
			
			KeywordUtil.logInfo("Set TestCase ExecuteStatus : " + testCaseStatus)
			excelKw.setValueToCellByIndex(sh, GlobalVariable.currentRowTestCase, 3, GlobalVariable.testCaseExecuteStatus)
			excelKw.saveWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName, xssfWb)
			KeywordUtil.logInfo("ReportResult Save : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
			GlobalVariable.currentRowTestCase = GlobalVariable.currentRowTestCase + 1
			KeywordUtil.logInfo("********************************************")
		}
		GlobalVariable.runningStep = 0
	}

	/**
	 * Executes before every test suite starts.
	 * @param testSuiteContext: related information of the executed test suite.
	 */
	@BeforeTestSuite
	def sampleBeforeTestSuite(TestSuiteContext testSuiteContext) {
//		println testSuiteContext.getTestSuiteId()
		
		KeywordUtil.logInfo("********* SampleBeforeTestSuite " + testSuiteContext.getTestSuiteId() + " *********")	
		util = new UtilityKeyword()
		String generateDateTime = util.FW_GenerateDateTime()
		GlobalVariable.dateExecuted = util.FW_DateExecuted()
		GlobalVariable.testStartTime = util.FW_TestStartTime()
		
		KeywordUtil.logInfo("********* Create Initial FrameWork *********")
		
		try {
			
			File myFile = new File(GlobalVariable.reportFolderName + generateDateTime);
			if (myFile.mkdir()) {
				GlobalVariable.outputReportFolderName = GlobalVariable.reportFolderName + generateDateTime + "\\"
				GlobalVariable.outputReportFileName = generateDateTime + "_" + GlobalVariable.outputReportFileName + ".xlsx"
				KeywordUtil.logInfo("ReportFolderName Created : " + GlobalVariable.outputReportFolderName)
				KeywordUtil.logInfo("ReportFileName Created : " + GlobalVariable.testStartTime)
				KeywordUtil.logInfo("File Created : " + myFile.getName())
				
				excelKw = new ExcelKeyword()
				xssfWb = excelKw.getWorkbook(GlobalVariable.excelController)
				KeywordUtil.logInfo("ReportResult Created : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
				
				sh = excelKw.getExcelSheet(xssfWb, "SummaryReport")
				excelKw.setValueToCellByIndex(sh, 1, 1, GlobalVariable.dateExecuted)
				excelKw.setValueToCellByIndex(sh, 2, 1, GlobalVariable.testStartTime)
				KeywordUtil.logInfo("Set DateExecuted : " + GlobalVariable.dateExecuted)
				KeywordUtil.logInfo("Set TestStartTime : " + GlobalVariable.testStartTime)
				excelKw.saveWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName, xssfWb)
				KeywordUtil.logInfo("ReportResult Save : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
			} else {
				KeywordUtil.logInfo("File Already Exists.")
			}

		} catch (IOException e) {
			KeywordUtil.logInfo("SampleBeforeTestSuite IOException : " + e.getMessage())
			e.printStackTrace();
		}
		
		KeywordUtil.logInfo("********************************************")
		
	}

	/**
	 * Executes after every test suite ends.
	 * @param testSuiteContext: related information of the executed test suite.
	 */
	@AfterTestSuite
	def sampleAfterTestSuite(TestSuiteContext testSuiteContext) {
//		println testSuiteContext.getTestSuiteId()
		
		KeywordUtil.logInfo("********* SampleAfterTestSuite " + testSuiteContext.getTestSuiteId() + " *********")
		GlobalVariable.testEndTime = util.FW_TestEndTime()
		xssfWb = excelKw.getWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		KeywordUtil.logInfo("ReportResult Open : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		sh = excelKw.getExcelSheet(xssfWb, "SummaryReport")
		excelKw.setValueToCellByIndex(sh, 3, 1, GlobalVariable.testEndTime)
		excelKw.setValueToCellByIndex(sh, 5, 1, GlobalVariable.totalTestCasesExecuted)
		excelKw.setValueToCellByIndex(sh, 6, 1, GlobalVariable.totalTestCasesPassed)
		excelKw.setValueToCellByIndex(sh, 7, 1, GlobalVariable.totalTestCasesFailed)
		KeywordUtil.logInfo("Set Test EndTime : " + GlobalVariable.testEndTime)
		KeywordUtil.logInfo("Set Total TestCases Executed : " + GlobalVariable.totalTestCasesExecuted)
		KeywordUtil.logInfo("Set Total TestCases Passed : " + GlobalVariable.totalTestCasesPassed)
		KeywordUtil.logInfo("Set Total TestCases Failed : " + GlobalVariable.totalTestCasesFailed)
		excelKw.saveWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName, xssfWb)
		KeywordUtil.logInfo("ReportResult Save : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		KeywordUtil.logInfo("********************************************")
	}
}