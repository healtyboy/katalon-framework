import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')
CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_OpenBrowser'('https://demostore.x-cart.com/', "เปิดเวป Demostore")
//WebUI.navigateToUrl('https://demostore.x-cart.com/')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_Click'(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Catalog/span_Sign in  sign up'), "กดปุ่ม  Sign in หรือ sign up")
//WebUI.click(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Catalog/span_Sign in  sign up'))
CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SetText'(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Catalog/input__login'), "กรอก Username", 'trithep.c@extosoft.com')
//WebUI.setText(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Catalog/input__login'), 
//    'trithep.c@extosoft.com')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SetText'(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Catalog/input__password'), "กรอก Password",'trithep@1234')
//WebUI.setEncryptedText(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Catalog/input__password'), 
//    '1nlTr6GAFlvAyzI/dLhN/A==')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_CloseBrowser'('https://demostore.x-cart.com/', "ปิดเวป Demostore")

//WebUI.click(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Catalog/span_Sign in'))
//
//WebUI.click(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Catalog/span_Bestsellers'))
//
//WebUI.click(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Bestsellers/span_Shoes'))
//
//WebUI.click(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Fashion  Shoes/a_Beige Sneakers'))
//
//WebUI.setText(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Shoes  Beig_680ede/input_Qty_amount'), 
//    '5')
//
//WebUI.click(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Shoes  Beig_680ede/span_Add to cart'))
//
//WebUI.click(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Shoes  Beig_680ede/span_View cart'))
//
//WebUI.click(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Your shoppi_e6ae8c/span_Go to checkout'))
//
//WebUI.click(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Secure Checkout/input__methodId'))
//
//WebUI.setText(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Secure Checkout/textarea_Order note_notes'), 
//    'trithep')
//
//WebUI.click(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Secure Checkout/button_Proceed to payment'))
//
//WebUI.click(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Secure Checkout/input_Phone (555) 555-5555_methodId'))
//
//WebUI.click(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Secure Checkout/button_Place order'))
//
//WebUI.click(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Thank you f_8d0f3e/h1_Thank you for your order'))
//
//WebUI.click(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Thank you f_8d0f3e/h1_Thank you for your order'))
//
//WebUI.click(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Thank you f_8d0f3e/h1_Thank you for your order'))
//
//WebUI.click(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Thank you f_8d0f3e/a_My account'))
//
//WebUI.click(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Thank you f_8d0f3e/span_Log out'))

