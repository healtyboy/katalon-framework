import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.extosoft.keyword.WebKeyword as WebKeyword
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import sample.Login.*
import sample.Shop.*
import sample.Checkout.*
import com.extosoft.keyword.WebKeyword.*

CustomKeywords.'sample.Login.loginIntoApplicationWithGlobalVariable'()

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_WaitForElementPresent'(findTestObject('Pages/Shop page/lnkShop'), "Pages/Shop page/lnkShop", GlobalVariable.waitPresentTimeout)

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_Click'(findTestObject('Pages/Shop page/lnkShop'), "Pages/Shop page/lnkShop")

CustomKeywords.'sample.Shop.addToCartWithGlobalVariable'()

CustomKeywords.'sample.Checkout.CheckoutShopWithGlobalVariable'()

CustomKeywords.'sample.Login.logoutFromApplication'()

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_CloseBrowser'(GlobalVariable.urlLogin)

