<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Run Test Controller</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>64fc548a-ef94-4e72-bb99-a3d839d7034f</testSuiteGuid>
   <testCaseLink>
      <guid>b5c7334a-970a-4806-949f-c047d30ee43b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC01</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>24b0821a-9327-498e-a829-fd82cab62835</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC02</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>568e5b0d-8224-4b62-9326-dd79ba805609</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC03</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
