package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object urlLogin
     
    /**
     * <p></p>
     */
    public static Object username
     
    /**
     * <p></p>
     */
    public static Object password
     
    /**
     * <p></p>
     */
    public static Object waitPresentTimeout
     
    /**
     * <p></p>
     */
    public static Object urlShop
     
    /**
     * <p></p>
     */
    public static Object companyName
     
    /**
     * <p></p>
     */
    public static Object address
     
    /**
     * <p></p>
     */
    public static Object city
     
    /**
     * <p></p>
     */
    public static Object country
     
    /**
     * <p></p>
     */
    public static Object postCode
     
    /**
     * <p></p>
     */
    public static Object Phone
     
    /**
     * <p></p>
     */
    public static Object uploadPlaceOrderTimeout
     
    /**
     * <p></p>
     */
    public static Object dataFile
     
    /**
     * <p></p>
     */
    public static Object inputColumHeader
     
    /**
     * <p></p>
     */
    public static Object urlProduct
     
    /**
     * <p></p>
     */
    public static Object productName
     
    /**
     * <p></p>
     */
    public static Object coupon
     
    /**
     * <p></p>
     */
    public static Object firstName
     
    /**
     * <p></p>
     */
    public static Object lastName
     
    /**
     * <p></p>
     */
    public static Object testStartTime
     
    /**
     * <p></p>
     */
    public static Object testEndTime
     
    /**
     * <p></p>
     */
    public static Object dateExecuted
     
    /**
     * <p></p>
     */
    public static Object reportFolderName
     
    /**
     * <p></p>
     */
    public static Object outputReportFolderName
     
    /**
     * <p></p>
     */
    public static Object outputReportFileName
     
    /**
     * <p></p>
     */
    public static Object excelController
     
    /**
     * <p></p>
     */
    public static Object currentTestCaseName
     
    /**
     * <p></p>
     */
    public static Object testCaseExecuteStatus
     
    /**
     * <p></p>
     */
    public static Object currentRowTestCase
     
    /**
     * <p></p>
     */
    public static Object testCaseName
     
    /**
     * <p></p>
     */
    public static Object tag
     
    /**
     * <p></p>
     */
    public static Object totalTestCasesPassed
     
    /**
     * <p></p>
     */
    public static Object totalTestCasesFailed
     
    /**
     * <p></p>
     */
    public static Object totalTestCasesExecuted
     
    /**
     * <p></p>
     */
    public static Object testCase
     
    /**
     * <p></p>
     */
    public static Object currentRowLogTransaction
     
    /**
     * <p></p>
     */
    public static Object testStartLogTransaction
     
    /**
     * <p></p>
     */
    public static Object currentRowStep
     
    /**
     * <p></p>
     */
    public static Object outputScreenShotFileName
     
    /**
     * <p></p>
     */
    public static Object stepName
     
    /**
     * <p></p>
     */
    public static Object transactionName
     
    /**
     * <p></p>
     */
    public static Object status
     
    /**
     * <p></p>
     */
    public static Object runningStep
     
    /**
     * <p></p>
     */
    public static Object stepValue
     
    /**
     * <p></p>
     */
    public static Object captureName
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            urlLogin = selectedVariables['urlLogin']
            username = selectedVariables['username']
            password = selectedVariables['password']
            waitPresentTimeout = selectedVariables['waitPresentTimeout']
            urlShop = selectedVariables['urlShop']
            companyName = selectedVariables['companyName']
            address = selectedVariables['address']
            city = selectedVariables['city']
            country = selectedVariables['country']
            postCode = selectedVariables['postCode']
            Phone = selectedVariables['Phone']
            uploadPlaceOrderTimeout = selectedVariables['uploadPlaceOrderTimeout']
            dataFile = selectedVariables['dataFile']
            inputColumHeader = selectedVariables['inputColumHeader']
            urlProduct = selectedVariables['urlProduct']
            productName = selectedVariables['productName']
            coupon = selectedVariables['coupon']
            firstName = selectedVariables['firstName']
            lastName = selectedVariables['lastName']
            testStartTime = selectedVariables['testStartTime']
            testEndTime = selectedVariables['testEndTime']
            dateExecuted = selectedVariables['dateExecuted']
            reportFolderName = selectedVariables['reportFolderName']
            outputReportFolderName = selectedVariables['outputReportFolderName']
            outputReportFileName = selectedVariables['outputReportFileName']
            excelController = selectedVariables['excelController']
            currentTestCaseName = selectedVariables['currentTestCaseName']
            testCaseExecuteStatus = selectedVariables['testCaseExecuteStatus']
            currentRowTestCase = selectedVariables['currentRowTestCase']
            testCaseName = selectedVariables['testCaseName']
            tag = selectedVariables['tag']
            totalTestCasesPassed = selectedVariables['totalTestCasesPassed']
            totalTestCasesFailed = selectedVariables['totalTestCasesFailed']
            totalTestCasesExecuted = selectedVariables['totalTestCasesExecuted']
            testCase = selectedVariables['testCase']
            currentRowLogTransaction = selectedVariables['currentRowLogTransaction']
            testStartLogTransaction = selectedVariables['testStartLogTransaction']
            currentRowStep = selectedVariables['currentRowStep']
            outputScreenShotFileName = selectedVariables['outputScreenShotFileName']
            stepName = selectedVariables['stepName']
            transactionName = selectedVariables['transactionName']
            status = selectedVariables['status']
            runningStep = selectedVariables['runningStep']
            stepValue = selectedVariables['stepValue']
            captureName = selectedVariables['captureName']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
