
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import org.apache.poi.ss.usermodel.Sheet

import java.lang.String

import java.lang.Object

import java.util.Map

import org.apache.poi.ss.usermodel.Workbook

import java.util.List

import org.apache.poi.ss.usermodel.Cell

import org.apache.poi.ss.usermodel.Row

import com.kms.katalon.core.testobject.TestObject


def static "com.kms.katalon.keyword.excel.ExcelKeywords.getRowIndexByCellContent"(
    	Sheet sheet	
     , 	String cellContent	
     , 	int columnIdxForCell	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getRowIndexByCellContent(
        	sheet
         , 	cellContent
         , 	columnIdxForCell)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getMapOfKeyValueRows"(
    	Sheet sheet	
     , 	int keyRowIdx	
     , 	int valueRowIdx	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getMapOfKeyValueRows(
        	sheet
         , 	keyRowIdx
         , 	valueRowIdx)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getCellValuesByRangeIndexes"(
    	Sheet sheet	
     , 	int rowInd1	
     , 	int colInd1	
     , 	int rowInd2	
     , 	int colInd2	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getCellValuesByRangeIndexes(
        	sheet
         , 	rowInd1
         , 	colInd1
         , 	rowInd2
         , 	colInd2)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getCellValueByRangeAddress"(
    	Sheet sheet	
     , 	String topLeftAddress	
     , 	String rightBottomAddress	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getCellValueByRangeAddress(
        	sheet
         , 	topLeftAddress
         , 	rightBottomAddress)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getExcelSheetByName"(
    	String filePath	
     , 	String sheetName	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getExcelSheetByName(
        	filePath
         , 	sheetName)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.setValueToCellByAddress"(
    	Sheet sheet	
     , 	String cellAddress	
     , 	Object value	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).setValueToCellByAddress(
        	sheet
         , 	cellAddress
         , 	value)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getCellIndexByAddress"(
    	Sheet sheet	
     , 	String cellAddress	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getCellIndexByAddress(
        	sheet
         , 	cellAddress)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.setValueToCellByAddresses"(
    	Sheet sheet	
     , 	Map content	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).setValueToCellByAddresses(
        	sheet
         , 	content)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getCellValueByAddress"(
    	Sheet sheet	
     , 	String cellAddress	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getCellValueByAddress(
        	sheet
         , 	cellAddress)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.setValueToCellByIndex"(
    	Sheet sheet	
     , 	int rowIndex	
     , 	int colIndex	
     , 	Object value	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).setValueToCellByIndex(
        	sheet
         , 	rowIndex
         , 	colIndex
         , 	value)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getCellValueByIndex"(
    	Sheet sheet	
     , 	int rowIdx	
     , 	int colIdx	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getCellValueByIndex(
        	sheet
         , 	rowIdx
         , 	colIdx)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.createExcelSheets"(
    	Workbook workbook	
     , 	java.util.List<String> sheetNames	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).createExcelSheets(
        	workbook
         , 	sheetNames)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getColumnsByIndex"(
    	Sheet sheet	
     , 	java.util.List<java.lang.Integer> columnIndexes	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getColumnsByIndex(
        	sheet
         , 	columnIndexes)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.createExcelFile"(
    	String filePath	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).createExcelFile(
        	filePath)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.createExcelSheet"(
    	Workbook workbook	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).createExcelSheet(
        	workbook)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.createExcelSheet"(
    	Workbook workbook	
     , 	String sheetName	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).createExcelSheet(
        	workbook
         , 	sheetName)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getSheetNames"(
    	Workbook workbook	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getSheetNames(
        	workbook)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getCellByIndex"(
    	Sheet sheet	
     , 	int rowIdx	
     , 	int colIdx	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getCellByIndex(
        	sheet
         , 	rowIdx
         , 	colIdx)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getWorkbook"(
    	String filePath	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getWorkbook(
        	filePath)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getCellByAddress"(
    	Sheet sheet	
     , 	String cellAddress	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getCellByAddress(
        	sheet
         , 	cellAddress)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.locateCell"(
    	Sheet sheet	
     , 	Object cellContent	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).locateCell(
        	sheet
         , 	cellContent)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.saveWorkbook"(
    	String filePath	
     , 	Workbook workbook	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).saveWorkbook(
        	filePath
         , 	workbook)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getExcelSheet"(
    	String filePath	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getExcelSheet(
        	filePath)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getExcelSheet"(
    	Workbook wbs	
     , 	String sheetName	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getExcelSheet(
        	wbs
         , 	sheetName)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getExcelSheet"(
    	String filePath	
     , 	int sheetIndex	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getExcelSheet(
        	filePath
         , 	sheetIndex)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getExcelSheet"(
    	Workbook wbs	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getExcelSheet(
        	wbs)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.createWorkbook"(
    	String filePath	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).createWorkbook(
        	filePath)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getTableContent"(
    	Sheet sheet	
     , 	int startRow	
     , 	int endRow	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getTableContent(
        	sheet
         , 	startRow
         , 	endRow)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getDataRows"(
    	Sheet sheet	
     , 	java.util.List<java.lang.Integer> rowIndexs	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getDataRows(
        	sheet
         , 	rowIndexs)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.getCellValue"(
    	Cell cell	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).getCellValue(
        	cell)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.compareTwoExcels"(
    	Workbook workbook1	
     , 	Workbook workbook2	
     , 	boolean isValueOnly	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).compareTwoExcels(
        	workbook1
         , 	workbook2
         , 	isValueOnly)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.compareTwoExcels"(
    	Workbook workbook1	
     , 	Workbook workbook2	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).compareTwoExcels(
        	workbook1
         , 	workbook2)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.compareTwoSheets"(
    	Sheet sheet1	
     , 	Sheet sheet2	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).compareTwoSheets(
        	sheet1
         , 	sheet2)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.compareTwoSheets"(
    	Sheet sheet1	
     , 	Sheet sheet2	
     , 	boolean isValueOnly	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).compareTwoSheets(
        	sheet1
         , 	sheet2
         , 	isValueOnly)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.compareTwoRows"(
    	Row row1	
     , 	Row row2	
     , 	boolean isValueOnly	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).compareTwoRows(
        	row1
         , 	row2
         , 	isValueOnly)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.compareTwoRows"(
    	Row row1	
     , 	Row row2	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).compareTwoRows(
        	row1
         , 	row2)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.compareTwoCells"(
    	Cell cell1	
     , 	Cell cell2	
     , 	boolean isValueOnly	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).compareTwoCells(
        	cell1
         , 	cell2
         , 	isValueOnly)
}

def static "com.kms.katalon.keyword.excel.ExcelKeywords.compareTwoCells"(
    	Cell cell1	
     , 	Cell cell2	) {
    (new com.kms.katalon.keyword.excel.ExcelKeywords()).compareTwoCells(
        	cell1
         , 	cell2)
}

def static "sample.Login.loginIntoApplication"(
    	String applicationURL	
     , 	String username	
     , 	String password	) {
    (new sample.Login()).loginIntoApplication(
        	applicationURL
         , 	username
         , 	password)
}

def static "sample.Login.loginIntoApplicationWithGlobalVariable"() {
    (new sample.Login()).loginIntoApplicationWithGlobalVariable()
}

def static "sample.Login.logoutFromApplication"() {
    (new sample.Login()).logoutFromApplication()
}

def static "com.extosoft.keyword.WebKeyword.FW_OpenBrowser"(
    	String applicationURL	
     , 	String objectName	) {
    (new com.extosoft.keyword.WebKeyword()).FW_OpenBrowser(
        	applicationURL
         , 	objectName)
}

def static "com.extosoft.keyword.WebKeyword.FW_CloseBrowser"(
    	String applicationURL	
     , 	String objectName	) {
    (new com.extosoft.keyword.WebKeyword()).FW_CloseBrowser(
        	applicationURL
         , 	objectName)
}

def static "com.extosoft.keyword.WebKeyword.FW_WaitForPageLoad"() {
    (new com.extosoft.keyword.WebKeyword()).FW_WaitForPageLoad()
}

def static "com.extosoft.keyword.WebKeyword.FW_WaitForPageLoad"(
    	int waitPresentTimeout	) {
    (new com.extosoft.keyword.WebKeyword()).FW_WaitForPageLoad(
        	waitPresentTimeout)
}

def static "com.extosoft.keyword.WebKeyword.FW_MaximizeWindow"() {
    (new com.extosoft.keyword.WebKeyword()).FW_MaximizeWindow()
}

def static "com.extosoft.keyword.WebKeyword.FW_WaitForElementVisible"(
    	TestObject testObject	
     , 	String objectName	) {
    (new com.extosoft.keyword.WebKeyword()).FW_WaitForElementVisible(
        	testObject
         , 	objectName)
}

def static "com.extosoft.keyword.WebKeyword.FW_WaitForElementVisible"(
    	TestObject testObject	
     , 	String objectName	
     , 	int waitPresentTimeout	) {
    (new com.extosoft.keyword.WebKeyword()).FW_WaitForElementVisible(
        	testObject
         , 	objectName
         , 	waitPresentTimeout)
}

def static "com.extosoft.keyword.WebKeyword.FW_WaitForElementPresent"(
    	TestObject testObject	
     , 	String objectName	) {
    (new com.extosoft.keyword.WebKeyword()).FW_WaitForElementPresent(
        	testObject
         , 	objectName)
}

def static "com.extosoft.keyword.WebKeyword.FW_WaitForElementPresent"(
    	TestObject testObject	
     , 	String objectName	
     , 	int waitPresentTimeout	) {
    (new com.extosoft.keyword.WebKeyword()).FW_WaitForElementPresent(
        	testObject
         , 	objectName
         , 	waitPresentTimeout)
}

def static "com.extosoft.keyword.WebKeyword.FW_WaitForElementNotPresent"(
    	TestObject testObject	
     , 	String objectName	
     , 	int waitNotPresentTimeout	) {
    (new com.extosoft.keyword.WebKeyword()).FW_WaitForElementNotPresent(
        	testObject
         , 	objectName
         , 	waitNotPresentTimeout)
}

def static "com.extosoft.keyword.WebKeyword.FW_VerifyElementPresent"(
    	TestObject testObject	
     , 	String objectName	) {
    (new com.extosoft.keyword.WebKeyword()).FW_VerifyElementPresent(
        	testObject
         , 	objectName)
}

def static "com.extosoft.keyword.WebKeyword.FW_VerifyElementPresent"(
    	TestObject testObject	
     , 	String objectName	
     , 	int waitPresentTimeout	) {
    (new com.extosoft.keyword.WebKeyword()).FW_VerifyElementPresent(
        	testObject
         , 	objectName
         , 	waitPresentTimeout)
}

def static "com.extosoft.keyword.WebKeyword.FW_SetText"(
    	TestObject testObject	
     , 	String objectName	
     , 	String value	) {
    (new com.extosoft.keyword.WebKeyword()).FW_SetText(
        	testObject
         , 	objectName
         , 	value)
}

def static "com.extosoft.keyword.WebKeyword.FW_SendKeys"(
    	TestObject testObject	
     , 	String objectName	
     , 	String value	) {
    (new com.extosoft.keyword.WebKeyword()).FW_SendKeys(
        	testObject
         , 	objectName
         , 	value)
}

def static "com.extosoft.keyword.WebKeyword.FW_Click"(
    	TestObject testObject	
     , 	String objectName	) {
    (new com.extosoft.keyword.WebKeyword()).FW_Click(
        	testObject
         , 	objectName)
}

def static "com.extosoft.keyword.WebKeyword.FW_Delay"(
    	int delayTime	) {
    (new com.extosoft.keyword.WebKeyword()).FW_Delay(
        	delayTime)
}

def static "com.extosoft.keyword.WebKeyword.refreshBrowser"() {
    (new com.extosoft.keyword.WebKeyword()).refreshBrowser()
}

def static "com.extosoft.keyword.WebKeyword.clickElement"(
    	TestObject to	) {
    (new com.extosoft.keyword.WebKeyword()).clickElement(
        	to)
}

def static "com.extosoft.keyword.WebKeyword.getHtmlTableRows"(
    	TestObject table	
     , 	String outerTagName	) {
    (new com.extosoft.keyword.WebKeyword()).getHtmlTableRows(
        	table
         , 	outerTagName)
}

def static "com.extosoft.keyword.WebKeyword.FW_WaitForElementPresent"(
    	TestObject testObject	
     , 	int waitPresentTimeout	) {
    (new com.extosoft.keyword.WebKeyword()).FW_WaitForElementPresent(
        	testObject
         , 	waitPresentTimeout)
}

def static "sample.Utils.clickOnSelect2"(
    	TestObject to	) {
    (new sample.Utils()).clickOnSelect2(
        	to)
}

def static "sample.Utils.findContainer"(
    	TestObject to	) {
    (new sample.Utils()).findContainer(
        	to)
}

def static "sample.Utils.selectResult"(
    	String option	
     , 	String subContainerOpenClass	) {
    (new sample.Utils()).selectResult(
        	option
         , 	subContainerOpenClass)
}

def static "sample.Utils.enterText"(
    	String option	) {
    (new sample.Utils()).enterText(
        	option)
}

def static "com.kms.katalon.keyword.pdf.PDF.compareAllPages"(
    	String file1	
     , 	String file2	
     , 	Object excludePattern	) {
    (new com.kms.katalon.keyword.pdf.PDF()).compareAllPages(
        	file1
         , 	file2
         , 	excludePattern)
}

def static "com.kms.katalon.keyword.pdf.PDF.saveAllPagesAsImages"(
    	String file	) {
    (new com.kms.katalon.keyword.pdf.PDF()).saveAllPagesAsImages(
        	file)
}

def static "com.kms.katalon.keyword.pdf.PDF.extractImagesFromPage"(
    	String file	
     , 	int startPage	) {
    (new com.kms.katalon.keyword.pdf.PDF()).extractImagesFromPage(
        	file
         , 	startPage)
}

def static "com.kms.katalon.keyword.pdf.PDF.getTextInPageRange"(
    	String file	
     , 	int startPage	
     , 	int endPage	) {
    (new com.kms.katalon.keyword.pdf.PDF()).getTextInPageRange(
        	file
         , 	startPage
         , 	endPage)
}

def static "com.kms.katalon.keyword.pdf.PDF.compareInPageRange"(
    	String file1	
     , 	String file2	
     , 	int startPage	
     , 	int endPage	
     , 	Object excludePattern	) {
    (new com.kms.katalon.keyword.pdf.PDF()).compareInPageRange(
        	file1
         , 	file2
         , 	startPage
         , 	endPage
         , 	excludePattern)
}

def static "com.kms.katalon.keyword.pdf.PDF.extractImagesInPageRange"(
    	String file	
     , 	int startPage	
     , 	int endPage	) {
    (new com.kms.katalon.keyword.pdf.PDF()).extractImagesInPageRange(
        	file
         , 	startPage
         , 	endPage)
}

def static "com.kms.katalon.keyword.pdf.PDF.savePageRangeAsImages"(
    	String file	
     , 	int startPage	
     , 	int endPage	) {
    (new com.kms.katalon.keyword.pdf.PDF()).savePageRangeAsImages(
        	file
         , 	startPage
         , 	endPage)
}

def static "com.kms.katalon.keyword.pdf.PDF.getPageNumber"(
    	String file	) {
    (new com.kms.katalon.keyword.pdf.PDF()).getPageNumber(
        	file)
}

def static "com.kms.katalon.keyword.pdf.PDF.compareByPixel"(
    	String file1	
     , 	String file2	
     , 	int startPage	
     , 	int endPage	
     , 	boolean highlightImageDifferences	
     , 	boolean showAllDifferences	) {
    (new com.kms.katalon.keyword.pdf.PDF()).compareByPixel(
        	file1
         , 	file2
         , 	startPage
         , 	endPage
         , 	highlightImageDifferences
         , 	showAllDifferences)
}

def static "com.kms.katalon.keyword.pdf.PDF.compareFromPage"(
    	String file1	
     , 	String file2	
     , 	int startPage	
     , 	Object excludePattern	) {
    (new com.kms.katalon.keyword.pdf.PDF()).compareFromPage(
        	file1
         , 	file2
         , 	startPage
         , 	excludePattern)
}

def static "com.kms.katalon.keyword.pdf.PDF.extractAllImages"(
    	String file	) {
    (new com.kms.katalon.keyword.pdf.PDF()).extractAllImages(
        	file)
}

def static "com.kms.katalon.keyword.pdf.PDF.savePageAsImage"(
    	String file	
     , 	int startPage	) {
    (new com.kms.katalon.keyword.pdf.PDF()).savePageAsImage(
        	file
         , 	startPage)
}

def static "com.kms.katalon.keyword.pdf.PDF.getTextFromPage"(
    	String file	
     , 	int startPage	) {
    (new com.kms.katalon.keyword.pdf.PDF()).getTextFromPage(
        	file
         , 	startPage)
}

def static "com.kms.katalon.keyword.pdf.PDF.getAllText"(
    	String file	) {
    (new com.kms.katalon.keyword.pdf.PDF()).getAllText(
        	file)
}

def static "sample.Checkout.CheckoutShop"(
    	String firstName	
     , 	String lastName	
     , 	String companyName	
     , 	String country	
     , 	String address	
     , 	String city	
     , 	String postCode	
     , 	String Phone	) {
    (new sample.Checkout()).CheckoutShop(
        	firstName
         , 	lastName
         , 	companyName
         , 	country
         , 	address
         , 	city
         , 	postCode
         , 	Phone)
}

def static "sample.Checkout.CheckoutShopWithGlobalVariable"() {
    (new sample.Checkout()).CheckoutShopWithGlobalVariable()
}

def static "sample.Select2.selectOptionByLabel"(
    	TestObject to	
     , 	String option	) {
    (new sample.Select2()).selectOptionByLabel(
        	to
         , 	option)
}

def static "sample.Select2.selectManyOptionsByLabel"(
    	TestObject to	
     , 	java.util.List<String> options	) {
    (new sample.Select2()).selectManyOptionsByLabel(
        	to
         , 	options)
}

def static "sample.Select2.getSelectedOptionsLabel"(
    	TestObject to	) {
    (new sample.Select2()).getSelectedOptionsLabel(
        	to)
}

def static "sample.Select2.getSelectedOptionsList"(
    	TestObject to	) {
    (new sample.Select2()).getSelectedOptionsList(
        	to)
}

def static "sample.Select2.getAllOptionsLabel"(
    	TestObject to	) {
    (new sample.Select2()).getAllOptionsLabel(
        	to)
}

def static "sample.Select2.removeOptions"(
    	TestObject to	
     , 	java.util.List<String> options	) {
    (new sample.Select2()).removeOptions(
        	to
         , 	options)
}

def static "sample.Shop.navigatetoDetailPage"(
    	String productName	
     , 	String urlProduct	) {
    (new sample.Shop()).navigatetoDetailPage(
        	productName
         , 	urlProduct)
}

def static "sample.Shop.addToCart"(
    	String productName	
     , 	String urlProduct	) {
    (new sample.Shop()).addToCart(
        	productName
         , 	urlProduct)
}

def static "sample.Shop.applyCouponAndAddToCart"(
    	String productName	
     , 	String urlProduct	
     , 	String coupon	) {
    (new sample.Shop()).applyCouponAndAddToCart(
        	productName
         , 	urlProduct
         , 	coupon)
}

def static "sample.Shop.addToCartWithGlobalVariable"() {
    (new sample.Shop()).addToCartWithGlobalVariable()
}

def static "sample.Shop.applyCouponAndAddToCartWithGlobalVariable"() {
    (new sample.Shop()).applyCouponAndAddToCartWithGlobalVariable()
}

def static "com.extosoft.keyword.ExcelKeyword.createExcelFile"(
    	String filePath	) {
    (new com.extosoft.keyword.ExcelKeyword()).createExcelFile(
        	filePath)
}

def static "com.extosoft.keyword.ExcelKeyword.createExcelSheet"(
    	Workbook workbook	) {
    (new com.extosoft.keyword.ExcelKeyword()).createExcelSheet(
        	workbook)
}

def static "com.extosoft.keyword.ExcelKeyword.createExcelSheet"(
    	Workbook workbook	
     , 	String sheetName	) {
    (new com.extosoft.keyword.ExcelKeyword()).createExcelSheet(
        	workbook
         , 	sheetName)
}

def static "com.extosoft.keyword.ExcelKeyword.getSheetNames"(
    	Workbook workbook	) {
    (new com.extosoft.keyword.ExcelKeyword()).getSheetNames(
        	workbook)
}

def static "com.extosoft.keyword.ExcelKeyword.createExcelSheets"(
    	Workbook workbook	
     , 	java.util.List<String> sheetNames	) {
    (new com.extosoft.keyword.ExcelKeyword()).createExcelSheets(
        	workbook
         , 	sheetNames)
}

def static "com.extosoft.keyword.ExcelKeyword.getWorkbook"(
    	String filePath	) {
    (new com.extosoft.keyword.ExcelKeyword()).getWorkbook(
        	filePath)
}

def static "com.extosoft.keyword.ExcelKeyword.createWorkbook"(
    	String filePath	) {
    (new com.extosoft.keyword.ExcelKeyword()).createWorkbook(
        	filePath)
}

def static "com.extosoft.keyword.ExcelKeyword.saveWorkbook"(
    	String filePath	
     , 	Workbook workbook	) {
    (new com.extosoft.keyword.ExcelKeyword()).saveWorkbook(
        	filePath
         , 	workbook)
}

def static "com.extosoft.keyword.ExcelKeyword.setValueToCellByIndex"(
    	Sheet sheet	
     , 	int rowIndex	
     , 	int colIndex	
     , 	Object value	) {
    (new com.extosoft.keyword.ExcelKeyword()).setValueToCellByIndex(
        	sheet
         , 	rowIndex
         , 	colIndex
         , 	value)
}

def static "com.extosoft.keyword.ExcelKeyword.setValueToCellByAddress"(
    	Sheet sheet	
     , 	String cellAddress	
     , 	Object value	) {
    (new com.extosoft.keyword.ExcelKeyword()).setValueToCellByAddress(
        	sheet
         , 	cellAddress
         , 	value)
}

def static "com.extosoft.keyword.ExcelKeyword.setValueToCellByAddresses"(
    	Sheet sheet	
     , 	Map content	) {
    (new com.extosoft.keyword.ExcelKeyword()).setValueToCellByAddresses(
        	sheet
         , 	content)
}

def static "com.extosoft.keyword.ExcelKeyword.getExcelSheet"(
    	String filePath	
     , 	int sheetIndex	) {
    (new com.extosoft.keyword.ExcelKeyword()).getExcelSheet(
        	filePath
         , 	sheetIndex)
}

def static "com.extosoft.keyword.ExcelKeyword.getExcelSheet"(
    	Workbook wbs	
     , 	String sheetName	) {
    (new com.extosoft.keyword.ExcelKeyword()).getExcelSheet(
        	wbs
         , 	sheetName)
}

def static "com.extosoft.keyword.ExcelKeyword.getExcelSheetByName"(
    	String filePath	
     , 	String sheetName	) {
    (new com.extosoft.keyword.ExcelKeyword()).getExcelSheetByName(
        	filePath
         , 	sheetName)
}

def static "com.extosoft.keyword.ExcelKeyword.getCellByIndex"(
    	Sheet sheet	
     , 	int rowIdx	
     , 	int colIdx	) {
    (new com.extosoft.keyword.ExcelKeyword()).getCellByIndex(
        	sheet
         , 	rowIdx
         , 	colIdx)
}

def static "com.extosoft.keyword.ExcelKeyword.getCellByAddress"(
    	Sheet sheet	
     , 	String cellAddress	) {
    (new com.extosoft.keyword.ExcelKeyword()).getCellByAddress(
        	sheet
         , 	cellAddress)
}

def static "com.extosoft.keyword.ExcelKeyword.locateCell"(
    	Sheet sheet	
     , 	Object cellContent	) {
    (new com.extosoft.keyword.ExcelKeyword()).locateCell(
        	sheet
         , 	cellContent)
}

def static "com.extosoft.keyword.ExcelKeyword.getCellValue"(
    	Cell cell	) {
    (new com.extosoft.keyword.ExcelKeyword()).getCellValue(
        	cell)
}

def static "com.extosoft.keyword.ExcelKeyword.getCellValueByIndex"(
    	Sheet sheet	
     , 	int rowIdx	
     , 	int colIdx	) {
    (new com.extosoft.keyword.ExcelKeyword()).getCellValueByIndex(
        	sheet
         , 	rowIdx
         , 	colIdx)
}

def static "com.extosoft.keyword.ExcelKeyword.getCellIndexByAddress"(
    	Sheet sheet	
     , 	String cellAddress	) {
    (new com.extosoft.keyword.ExcelKeyword()).getCellIndexByAddress(
        	sheet
         , 	cellAddress)
}

def static "com.extosoft.keyword.ExcelKeyword.getCellValueByAddress"(
    	Sheet sheet	
     , 	String cellAddress	) {
    (new com.extosoft.keyword.ExcelKeyword()).getCellValueByAddress(
        	sheet
         , 	cellAddress)
}

def static "com.extosoft.keyword.ExcelKeyword.getRowIndexByCellContent"(
    	Sheet sheet	
     , 	String cellContent	
     , 	int columnIdxForCell	) {
    (new com.extosoft.keyword.ExcelKeyword()).getRowIndexByCellContent(
        	sheet
         , 	cellContent
         , 	columnIdxForCell)
}

def static "com.extosoft.keyword.ExcelKeyword.getTableContent"(
    	Sheet sheet	
     , 	int startRow	
     , 	int endRow	) {
    (new com.extosoft.keyword.ExcelKeyword()).getTableContent(
        	sheet
         , 	startRow
         , 	endRow)
}

def static "com.extosoft.keyword.ExcelKeyword.getDataRows"(
    	Sheet sheet	
     , 	java.util.List<java.lang.Integer> rowIndexs	) {
    (new com.extosoft.keyword.ExcelKeyword()).getDataRows(
        	sheet
         , 	rowIndexs)
}

def static "com.extosoft.keyword.ExcelKeyword.getMapOfKeyValueRows"(
    	Sheet sheet	
     , 	int keyRowIdx	
     , 	int valueRowIdx	) {
    (new com.extosoft.keyword.ExcelKeyword()).getMapOfKeyValueRows(
        	sheet
         , 	keyRowIdx
         , 	valueRowIdx)
}

def static "com.extosoft.keyword.ExcelKeyword.getCellValueByRangeAddress"(
    	Sheet sheet	
     , 	String topLeftAddress	
     , 	String rightBottomAddress	) {
    (new com.extosoft.keyword.ExcelKeyword()).getCellValueByRangeAddress(
        	sheet
         , 	topLeftAddress
         , 	rightBottomAddress)
}

def static "com.extosoft.keyword.ExcelKeyword.getCellValuesByRangeIndexes"(
    	Sheet sheet	
     , 	int rowInd1	
     , 	int colInd1	
     , 	int rowInd2	
     , 	int colInd2	) {
    (new com.extosoft.keyword.ExcelKeyword()).getCellValuesByRangeIndexes(
        	sheet
         , 	rowInd1
         , 	colInd1
         , 	rowInd2
         , 	colInd2)
}

def static "com.extosoft.keyword.ExcelKeyword.getColumnsByIndex"(
    	Sheet sheet	
     , 	java.util.List<java.lang.Integer> columnIndexes	) {
    (new com.extosoft.keyword.ExcelKeyword()).getColumnsByIndex(
        	sheet
         , 	columnIndexes)
}

def static "com.extosoft.keyword.ExcelKeyword.compareTwoExcels"(
    	Workbook workbook1	
     , 	Workbook workbook2	
     , 	boolean isValueOnly	) {
    (new com.extosoft.keyword.ExcelKeyword()).compareTwoExcels(
        	workbook1
         , 	workbook2
         , 	isValueOnly)
}

def static "com.extosoft.keyword.ExcelKeyword.compareTwoSheets"(
    	Sheet sheet1	
     , 	Sheet sheet2	
     , 	boolean isValueOnly	) {
    (new com.extosoft.keyword.ExcelKeyword()).compareTwoSheets(
        	sheet1
         , 	sheet2
         , 	isValueOnly)
}

def static "com.extosoft.keyword.ExcelKeyword.compareTwoRows"(
    	Row row1	
     , 	Row row2	
     , 	boolean isValueOnly	) {
    (new com.extosoft.keyword.ExcelKeyword()).compareTwoRows(
        	row1
         , 	row2
         , 	isValueOnly)
}

def static "com.extosoft.keyword.ExcelKeyword.compareTwoCells"(
    	Cell cell1	
     , 	Cell cell2	
     , 	boolean isValueOnly	) {
    (new com.extosoft.keyword.ExcelKeyword()).compareTwoCells(
        	cell1
         , 	cell2
         , 	isValueOnly)
}

def static "com.extosoft.keyword.ExcelKeyword.getExcelSheet"(
    	String filePath	) {
    (new com.extosoft.keyword.ExcelKeyword()).getExcelSheet(
        	filePath)
}

def static "com.extosoft.keyword.ExcelKeyword.getExcelSheet"(
    	Workbook wbs	) {
    (new com.extosoft.keyword.ExcelKeyword()).getExcelSheet(
        	wbs)
}

def static "com.extosoft.keyword.ExcelKeyword.compareTwoExcels"(
    	Workbook workbook1	
     , 	Workbook workbook2	) {
    (new com.extosoft.keyword.ExcelKeyword()).compareTwoExcels(
        	workbook1
         , 	workbook2)
}

def static "com.extosoft.keyword.ExcelKeyword.compareTwoSheets"(
    	Sheet sheet1	
     , 	Sheet sheet2	) {
    (new com.extosoft.keyword.ExcelKeyword()).compareTwoSheets(
        	sheet1
         , 	sheet2)
}

def static "com.extosoft.keyword.ExcelKeyword.compareTwoRows"(
    	Row row1	
     , 	Row row2	) {
    (new com.extosoft.keyword.ExcelKeyword()).compareTwoRows(
        	row1
         , 	row2)
}

def static "com.extosoft.keyword.ExcelKeyword.compareTwoCells"(
    	Cell cell1	
     , 	Cell cell2	) {
    (new com.extosoft.keyword.ExcelKeyword()).compareTwoCells(
        	cell1
         , 	cell2)
}

def static "com.extosoft.keyword.UtilityKeyword.FW_GenerateDateTime"() {
    (new com.extosoft.keyword.UtilityKeyword()).FW_GenerateDateTime()
}

def static "com.extosoft.keyword.UtilityKeyword.FW_TestStartTime"() {
    (new com.extosoft.keyword.UtilityKeyword()).FW_TestStartTime()
}

def static "com.extosoft.keyword.UtilityKeyword.FW_TestEndTime"() {
    (new com.extosoft.keyword.UtilityKeyword()).FW_TestEndTime()
}

def static "com.extosoft.keyword.UtilityKeyword.FW_DateExecuted"() {
    (new com.extosoft.keyword.UtilityKeyword()).FW_DateExecuted()
}

def static "com.extosoft.keyword.UtilityKeyword.FW_TestStartStep"(
    	String stepName	) {
    (new com.extosoft.keyword.UtilityKeyword()).FW_TestStartStep(
        	stepName)
}

def static "com.extosoft.keyword.UtilityKeyword.FW_TestStartStep"(
    	String stepName	
     , 	String stepValue	) {
    (new com.extosoft.keyword.UtilityKeyword()).FW_TestStartStep(
        	stepName
         , 	stepValue)
}

def static "com.extosoft.keyword.UtilityKeyword.FW_TestEndStep"() {
    (new com.extosoft.keyword.UtilityKeyword()).FW_TestEndStep()
}

def static "com.extosoft.keyword.UtilityKeyword.FW_CaptureScreenShot"() {
    (new com.extosoft.keyword.UtilityKeyword()).FW_CaptureScreenShot()
}
