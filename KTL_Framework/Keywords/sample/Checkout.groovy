package sample

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.extosoft.keyword.UtilityKeyword
import com.extosoft.keyword.WebKeyword
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable


public class Checkout {

//	def UtilityKeyword util = new UtilityKeyword()
	def WebKeyword web = new WebKeyword()
	def Select2 select2 = new Select2()
	def BlockUIDismissed blockUIDismissed = new BlockUIDismissed()

	@Keyword
	def void CheckoutShop(String firstName,String lastName,String companyName,String country,String address,String city,String postCode,String Phone){
//		util.FW_TestStartTransaction("CheckoutShop")
		//		WebUI.click(findTestObject('Pages/Checkout page/lnkCheckout'))
		web.FW_Click(findTestObject('Pages/Checkout page/lnkCheckout'), "Pages/Checkout page/lnkCheckout")
		//		WebUI.waitForElementVisible(findTestObject('Pages/Checkout page/txtFirstname'), GlobalVariable.waitPresentTimeout)
		web.FW_WaitForElementVisible(findTestObject('Pages/Checkout page/txtFirstname'), "Pages/Checkout page/txtFirstname", GlobalVariable.waitPresentTimeout)
		//		WebUI.setText(findTestObject('Pages/Checkout page/txtFirstname'), firstName)
		web.FW_SetText(findTestObject('Pages/Checkout page/txtFirstname'), "Pages/Checkout page/txtFirstname", firstName)
		//		WebUI.setText(findTestObject('Pages/Checkout page/txtLastname'), lastName)
		web.FW_SetText(findTestObject('Pages/Checkout page/txtLastname'), "Pages/Checkout page/txtLastname", lastName)
		//		WebUI.waitForElementVisible(findTestObject('Pages/Checkout page/inputCompanyName'), GlobalVariable.waitPresentTimeout)
		web.FW_WaitForElementVisible(findTestObject('Pages/Checkout page/txtFirstname'), "Pages/Checkout page/txtFirstname", GlobalVariable.waitPresentTimeout)
		//		WebUI.setText(findTestObject('Pages/Checkout page/inputCompanyName'), companyName)
		web.FW_SetText(findTestObject('Pages/Checkout page/inputCompanyName'), "Pages/Checkout page/inputCompanyName", companyName)

		select2.selectOptionByLabel(findTestObject('Select2/select_single'), country)
		select2.getAllOptionsLabel(findTestObject('Select2/select_single'))
		select2.getSelectedOptionsLabel(findTestObject('Select2/select_single'))

		//		WebUI.setText(findTestObject('Pages/Checkout page/inputAddress'), address)
		web.FW_SetText(findTestObject('Pages/Checkout page/inputAddress'), "Pages/Checkout page/inputAddress", address)
		//		WebUI.setText(findTestObject('Pages/Checkout page/inputCity'), city)
		web.FW_SetText(findTestObject('Pages/Checkout page/inputCity'), "Pages/Checkout page/inputCity", city)
		//		WebUI.setText(findTestObject('Pages/Checkout page/inputPostcode'), postCode)
		web.FW_SetText(findTestObject('Pages/Checkout page/inputPostcode'), "Pages/Checkout page/inputPostcode", postCode)
		//		WebUI.setText(findTestObject('Pages/Checkout page/inputPhone'), Phone)
		web.FW_SetText(findTestObject('Pages/Checkout page/inputPhone'), "Pages/Checkout page/inputPhone", Phone)

		blockUIDismissed.WaitBlockUIDismissed()
		//		WebUI.click(findTestObject('Pages/Checkout page/btnPlaceOrder'))
		web.FW_Click(findTestObject('Pages/Checkout page/btnPlaceOrder'), "Pages/Checkout page/btnPlaceOrder")
		blockUIDismissed.WaitBlockUIDismissed()

//		util.FW_TestEndTransaction()
	}

	@Keyword
	def void CheckoutShopWithGlobalVariable(){
		CheckoutShop(GlobalVariable.firstName,GlobalVariable.lastName,GlobalVariable.companyName, GlobalVariable.country, GlobalVariable.address, GlobalVariable.city, GlobalVariable.postCode, GlobalVariable.Phone)
	}
}
