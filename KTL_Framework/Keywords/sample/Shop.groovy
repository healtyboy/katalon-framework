package sample


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.extosoft.keyword.UtilityKeyword
import com.extosoft.keyword.WebKeyword
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class Shop {

//	def UtilityKeyword util = new UtilityKeyword()
	def WebKeyword web = new WebKeyword()

	@Keyword
	def void navigatetoDetailPage(String productName,String urlProduct){
		String temp = '/' + productName.replaceAll(" ", "-").toLowerCase()
		String urlDetail = urlProduct + temp
		WebUI.navigateToUrl(urlDetail)
	}

	@Keyword
	def void addToCart(String productName,String urlProduct){
//		util.FW_TestStartTransaction("AddToCart")
		navigatetoDetailPage(productName, urlProduct)
		//		WebUI.waitForElementPresent(findTestObject('Pages/Shop page/btnAddToCart'), GlobalVariable.waitPresentTimeout)
		web.FW_WaitForElementPresent(findTestObject('Pages/Shop page/btnAddToCart'), "Pages/Shop page/btnAddToCart", GlobalVariable.waitPresentTimeout)
		//		WebUI.click(findTestObject('Pages/Shop page/btnAddToCart'))
		web.FW_Click(findTestObject('Pages/Shop page/btnAddToCart'), "Pages/Shop page/btnAddToCart")
		//		WebUI.waitForElementPresent(findTestObject('Pages/Shop page/lnkViewCart'), GlobalVariable.waitPresentTimeout)
		web.FW_WaitForElementPresent(findTestObject('Pages/Shop page/lnkViewCart'), "Pages/Shop page/lnkViewCart", GlobalVariable.waitPresentTimeout)
		//		WebUI.click(findTestObject('Pages/Shop page/lnkViewCart'))
		web.FW_Click(findTestObject('Pages/Shop page/lnkViewCart'), "Pages/Shop page/lnkViewCart")
		//		WebUI.verifyElementPresent(findTestObject('Pages/Shop page/btnProceed'), GlobalVariable.waitPresentTimeout)
		web.FW_VerifyElementPresent(findTestObject('Pages/Shop page/btnProceed'), "Pages/Shop page/btnProceed", GlobalVariable.waitPresentTimeout)
//		util.FW_TestEndTransaction()
	}

	@Keyword
	def void applyCouponAndAddToCart(String productName,String urlProduct,String coupon){
//		util.FW_TestStartTransaction("ApplyCouponAndAddToCart")
		navigatetoDetailPage(productName, urlProduct)
		//		WebUI.delay(5)
		web.FW_Delay(5)
		//		WebUI.waitForElementPresent(findTestObject('Pages/Shop page/btnAddToCart'), GlobalVariable.waitPresentTimeout)
		web.FW_WaitForElementPresent(findTestObject('Pages/Shop page/btnAddToCart'), "Pages/Shop page/btnAddToCart", GlobalVariable.waitPresentTimeout)
		//		WebUI.click(findTestObject('Pages/Shop page/btnAddToCart'))
		web.FW_Click(findTestObject('Pages/Shop page/btnAddToCart'), "Pages/Shop page/btnAddToCart")
		//		WebUI.waitForElementPresent(findTestObject('Pages/Shop page/lnkViewCart'), GlobalVariable.waitPresentTimeout)
		web.FW_WaitForElementPresent(findTestObject('Pages/Shop page/lnkViewCart'), "Pages/Shop page/lnkViewCart", GlobalVariable.waitPresentTimeout)
		//		WebUI.click(findTestObject('Pages/Shop page/lnkViewCart'))
		web.FW_Click(findTestObject('Pages/Shop page/lnkViewCart'), "Pages/Shop page/lnkViewCart")
		//		WebUI.setText(findTestObject('Pages/Shop page/txtCoupon'), coupon)
		web.FW_SetText(findTestObject('Pages/Shop page/txtCoupon'), "Pages/Shop page/txtCoupon", coupon)
		//		WebUI.click(findTestObject('Pages/Shop page/btnApply'))
		web.FW_Click(findTestObject('Pages/Shop page/btnApply'), "Pages/Shop page/btnApply")
//		util.FW_TestEndTransaction()
	}

	@Keyword
	def void addToCartWithGlobalVariable(){
		addToCart(GlobalVariable.productName,GlobalVariable.urlProduct)
	}

	@Keyword
	def void applyCouponAndAddToCartWithGlobalVariable(){
		applyCouponAndAddToCart(GlobalVariable.productName, GlobalVariable.urlProduct, GlobalVariable.coupon)
	}
}
