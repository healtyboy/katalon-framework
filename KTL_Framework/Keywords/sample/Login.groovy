package sample


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.extosoft.keyword.UtilityKeyword
import com.extosoft.keyword.WebKeyword
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable
/*
 * Open browser
 * Navigate to URL -> get to Global variable
 * Provide username and password
 * Click on Login
 * */
/*
 * Will read value from Global Variable
 * */
public class Login {

//	def UtilityKeyword util = new UtilityKeyword()
	def WebKeyword web = new WebKeyword()
	@Keyword
	def void loginIntoApplication(String applicationURL,String username,String password){
//		util.FW_TestStartTransaction("LoginIntoApplication")
		//		WebUI.openBrowser(applicationURL)
		web.FW_OpenBrowser(applicationURL)
		//		WebUI.waitForPageLoad(GlobalVariable.waitPresentTimeout)
		web.FW_WaitForPageLoad(GlobalVariable.waitPresentTimeout)
		//		WebUI.maximizeWindow()
		web.FW_MaximizeWindow()
		//		WebUI.waitForElementVisible(findTestObject('Pages/MyAccount page/nav_HomeMyaccount'), GlobalVariable.waitPresentTimeout)
		web.FW_WaitForElementVisible(findTestObject('Pages/MyAccount page/nav_HomeMyaccount'), "Pages/MyAccount page/nav_HomeMyaccount", GlobalVariable.waitPresentTimeout)
		//		WebUI.sendKeys(findTestObject('Pages/MyAccount page/txtUsername'), username)
		web.FW_SendKeys(findTestObject('Pages/MyAccount page/txtUsername'), "Pages/MyAccount page/txtUsername", username)
		//		WebUI.sendKeys(findTestObject('Pages/MyAccount page/txtPassword'), password)
		web.FW_SendKeys(findTestObject('Pages/MyAccount page/txtPassword'), "Pages/MyAccount page/txtPassword", password)
		//		WebUI.click(findTestObject('Pages/MyAccount page/btnLogin'))
		web.FW_Click(findTestObject('Pages/MyAccount page/btnLogin'), "Pages/MyAccount page/btnLogin")
		//		WebUI.delay(1)
		web.FW_Delay(1)
//		util.FW_TestEndTransaction()
	}

	@Keyword
	def void loginIntoApplicationWithGlobalVariable(){
		loginIntoApplication(GlobalVariable.urlLogin, GlobalVariable.username, GlobalVariable.password)
	}

	@Keyword
	def void logoutFromApplication(){
//		util.FW_TestStartTransaction("LogoutFromApplication")
		//		WebUI.waitForElementPresent(findTestObject('Pages/MyAccount page/lnkMyAccount'), GlobalVariable.waitPresentTimeout)
		web.FW_WaitForElementPresent(findTestObject('Pages/MyAccount page/lnkMyAccount'), "Pages/MyAccount page/lnkMyAccount", GlobalVariable.waitPresentTimeout)
		//		WebUI.click(findTestObject('Pages/MyAccount page/lnkMyAccount'))
		web.FW_Click(findTestObject('Pages/MyAccount page/lnkMyAccount'), "Pages/MyAccount page/lnkMyAccount")
		//		WebUI.waitForElementPresent(findTestObject('Pages/MyAccount page/lnkLogout'), GlobalVariable.waitPresentTimeout)
		web.FW_WaitForElementPresent(findTestObject('Pages/MyAccount page/lnkLogout'), "Pages/MyAccount page/lnkLogout", GlobalVariable.waitPresentTimeout)
		//		WebUI.click(findTestObject('Pages/MyAccount page/lnkLogout'))
		web.FW_Click(findTestObject('Pages/MyAccount page/lnkLogout'), "Pages/MyAccount page/lnkLogout")
		//		WebUI.waitForElementVisible(findTestObject('Pages/MyAccount page/nav_HomeMyaccount'), GlobalVariable.waitPresentTimeout)
		web.FW_WaitForElementPresent(findTestObject('Pages/MyAccount page/nav_HomeMyaccount'), "Pages/MyAccount page/nav_HomeMyaccount", GlobalVariable.waitPresentTimeout)
//		util.FW_TestEndTransaction()
	}
}
