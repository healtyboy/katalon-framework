package com.extosoft.keyword
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException


class WebKeyword {

	def UtilityKeyword util = new UtilityKeyword()

	@Keyword
	def void FW_OpenBrowser(String applicationURL, String objectName) {
		KeywordUtil.logInfo("OpenBrowser : " + applicationURL)
		util.FW_TestStartStep(objectName)
		WebUI.openBrowser(applicationURL)
		FW_MaximizeWindow()
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_CloseBrowser(String applicationURL, String objectName) {
		KeywordUtil.logInfo("CloseBrowser : " + applicationURL)
		util.FW_TestStartStep(objectName)
		WebUI.closeBrowser()
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_WaitForPageLoad() {
		KeywordUtil.logInfo("FW_WaitForPageLoad : " + GlobalVariable.waitPresentTimeout)
		util.FW_TestStartStep("Page Loading")
		WebUI.waitForPageLoad(GlobalVariable.waitPresentTimeout)
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_WaitForPageLoad(int waitPresentTimeout) {
		KeywordUtil.logInfo("FW_WaitForPageLoad : " + waitPresentTimeout)
		util.FW_TestStartStep("Page Loading : " + waitPresentTimeout)
		WebUI.waitForPageLoad(waitPresentTimeout)
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_MaximizeWindow() {
		KeywordUtil.logInfo("FW_MaximizeWindow")
		//util.FW_TestStartStep("Maximize Size Window")
		WebUI.maximizeWindow()
		util.FW_CaptureScreenShot()
		//util.FW_TestEndStep()
	}

	@Keyword
	def void FW_WaitForElementVisible(TestObject testObject, String objectName) {
		KeywordUtil.logInfo("FW_WaitForElementVisible")
		util.FW_TestStartStep(objectName)
		WebUI.waitForElementVisible(testObject, GlobalVariable.waitPresentTimeout)
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_WaitForElementVisible(TestObject testObject, String objectName, int waitPresentTimeout) {
		KeywordUtil.logInfo("FW_WaitForElementVisible")
		util.FW_TestStartStep(objectName)
		WebUI.waitForElementVisible(testObject, waitPresentTimeout)
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_WaitForElementPresent(TestObject testObject, String objectName) {
		KeywordUtil.logInfo("FW_WaitForElementPresent")
		util.FW_TestStartStep(objectName)
		WebUI.waitForElementPresent(testObject, GlobalVariable.waitPresentTimeout)
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_WaitForElementPresent(TestObject testObject, String objectName, int waitPresentTimeout) {
		KeywordUtil.logInfo("FW_WaitForElementPresent")
		util.FW_TestStartStep(objectName)
		WebUI.waitForElementPresent(testObject, waitPresentTimeout)
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_WaitForElementNotPresent(TestObject testObject, String objectName, int waitNotPresentTimeout) {
		KeywordUtil.logInfo("FW_WaitForElementNotPresent")
		util.FW_TestStartStep(objectName)
		WebUI.waitForElementNotPresent(testObject, waitNotPresentTimeout)
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_VerifyElementPresent(TestObject testObject, String objectName) {
		KeywordUtil.logInfo("FW_VerifyElementPresent")
		util.FW_TestStartStep(objectName)
		WebUI.verifyElementPresent(testObject, GlobalVariable.waitPresentTimeout)
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_VerifyElementPresent(TestObject testObject, String objectName, int waitPresentTimeout) {
		KeywordUtil.logInfo("FW_VerifyElementPresent")
		util.FW_TestStartStep(objectName)
		WebUI.verifyElementPresent(testObject, waitPresentTimeout)
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_SetText(TestObject testObject, String objectName, String value) {
		KeywordUtil.logInfo("FW_SetText")
		util.FW_TestStartStep(objectName, value)
		WebUI.setText(testObject, value)
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_SendKeys(TestObject testObject, String objectName, String value) {
		KeywordUtil.logInfo("FW_SendKeys")
		util.FW_TestStartStep(objectName, value)
		WebUI.sendKeys(testObject, value)
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_Click(TestObject testObject, String objectName) {
		KeywordUtil.logInfo("FW_Click")
		util.FW_TestStartStep(objectName)
		WebUI.click(testObject)
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_Delay(int delayTime) {
		KeywordUtil.logInfo("FW_Delay")
		util.FW_TestStartStep("Delay", "")
		WebUI.delay(delayTime)
		util.FW_TestEndStep()
	}


	/**
	 * Refresh browser
	 */
	@Keyword
	def refreshBrowser() {
		KeywordUtil.logInfo("Refreshing")
		WebDriver webDriver = DriverFactory.getWebDriver()
		webDriver.navigate().refresh()
		KeywordUtil.markPassed("Refresh successfully")
	}

	/**
	 * Click element
	 * @param to Katalon test object
	 */
	@Keyword
	def clickElement(TestObject to) {
		try {
			WebElement element = WebUiBuiltInKeywords.findWebElement(to);
			KeywordUtil.logInfo("Clicking element")
			element.click()
			KeywordUtil.markPassed("Element has been clicked")
		} catch (WebElementNotFoundException e) {
			KeywordUtil.markFailed("Element not found")
		} catch (Exception e) {
			KeywordUtil.markFailed("Fail to click on element")
		}
	}

	/**
	 * Get all rows of HTML table
	 * @param table Katalon test object represent for HTML table
	 * @param outerTagName outer tag name of TR tag, usually is TBODY
	 * @return All rows inside HTML table
	 */
	@Keyword
	def List<WebElement> getHtmlTableRows(TestObject table, String outerTagName) {
		WebElement mailList = WebUiBuiltInKeywords.findWebElement(table)
		List<WebElement> selectedRows = mailList.findElements(By.xpath("./" + outerTagName + "/tr"))
		return selectedRows
	}

	/**
	 * Refresh browser
	 */
	@Keyword
	def FW_WaitForElementPresent(TestObject testObject, int waitPresentTimeout) {
		KeywordUtil.logInfo("FW_WaitForElementPresent")
		util.FW_TestStartStep("FW_WaitForElementPresent", "")
		WebUI.waitForElementPresent(testObject, waitPresentTimeout)
		util.FW_TestEndStep()
	}
}